library(tidyverse)
library(lubridate)
library(emo)
library(data.table)
library(stringr)

rm(list = ls())
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
loc <- fread("C:/Users/denis/Documents/projet_R/tidytuesday/data/2019/2019-11-12/loc_cran_packages.csv",encoding = "UTF-8") 

loc[,.N,by = pkg_name]$N %>% table()

loc[,uniqueN(version),by = pkg_name]$V1 %>% table()

# violin plot

#get the 20th
pkg_select <- loc[,.N,by = .(language)] %>%
  .[order(-N)]%>%
  .[1:20]
loc_plot <- loc[language %in% pkg_select$language]%>%
  .[,language := factor(language,level = pkg_select[order(N),language] )]


p <- ggplot(loc_plot[!language %in% c("JSON","Markdown")])+
  geom_violin(aes(as.factor(language),comment/code,color =as.factor(language), fill = as.factor(language)),scale = "width")+
  # geom_boxplot(aes(as.factor(language),comment/code,color = as.factor(language)),notch = T)+
  scale_y_log10()+
  # coord_cartesian(xlim = c(0,20), # This focuses the x-axis on the range of interest
  #                 clip = 'off') +
  coord_flip()+
  theme_bw()+
  labs(x = "language",
       y = "ratio comment over code",
       title  = "Comment over code lines ratio",
       subtitle = "How well is the code commented, for the most used languages in R packages. \r
       JSON and Markdown were removed because they had no comment\n
       ")+
  # scale_fill_brewer(type = "seq", palette = 1, direction = 1,
  #                   aesthetics = "fill")
  scale_fill_hue(h = c(0, 180) , c = 150, l = 70,
                   h.start = 0, direction = 1, na.value = "grey50")+
  scale_color_hue(h = c(0, 180) , c = 150, l = 70,
                 h.start = 0, direction = 1, na.value = "grey50")+
  theme(plot.title =  element_text(size = 20, color = "white",hjust = 0.5),
        plot.subtitle =  element_text(size = 15, color = "white",hjust = 0.5),
        plot.background = element_rect(fill  = "black"),
        panel.background = element_rect(fill  = "black"),
        legend.background = element_rect(fill  = "black"),
        panel.grid = element_line(colour  = "white"),
        axis.line = element_blank(),
        axis.ticks = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_text(size = 14, color = "white"),
        legend.position = "none",
        # plot.subtitle = element_text(size = 18,family = "Elephant", color = "red"),
        axis.text = element_text(size = 12,face = "bold", color = "white"))+
  annotate(geom = "text",
           x = 1.5, y = 30,
           label = "Lot of comments",
           hjust = "left",
           color="white")+
  annotate(geom = "text",
           x = 1.5, y = 0.0001,
           label = "Almost no comments",
           hjust = "left",
           color="white")
p  


ggsave(filename = "Rpackage.png",p)

# treemap ####

colors <- ggplot_build(p)$data[[1]]$fill %>% unique()
loc_tre <- loc[,.(Nline = sum(code),comratio = sum(comment)/sum(code)),by = language]
loc_tre_plot <- loc_tre[language %in% pkg_select$language & !language %in% c("JSON","Markdown")]
loc_tre_plot <- loc_tre[language %in% pkg_select$language ]
library(plyr)
loc_tre_plot[,language := mapvalues(language,c("Bourne Again Shell","Bourne Shell"),c("BAS","BS"))]
library(treemapify)

t <- ggplot(loc_tre_plot,
            aes(area = Nline,fill = comratio,label = language))+
  geom_treemap(colour = "black",size = 2)+
  scale_fill_gradient(low = "red", high = "green", trans = "log10",lim = c(0.0001,1))+
  geom_treemap_text(fontface = "bold", colour = "black", place = "centre",
                    grow = TRUE)+
  labs(fill = "comment/code \ntotal ratio",
       title = "Code lines and comments in R packages",
       subtitle = "Size is total number of code lines\ncolor is the total ratio comments/code lines\n")+
  theme(plot.title =  element_text(size = 20, color = "white",hjust = 0.5),
        plot.subtitle =  element_text(size = 15, color = "white",hjust = 0.5),
    plot.background = element_rect(fill  = "black"),
        panel.background = element_rect(fill  = "black"),
        legend.background = element_rect(fill  = "black"),
        panel.grid = element_line(colour  = "white"),
        axis.line = element_blank(),
        axis.ticks = element_blank(),
        axis.title = element_blank(),
        axis.text.x  = element_blank(),
        axis.text.y  = element_blank(),
        legend.text =  element_text(size = 12, color = "white"),
        legend.title  =  element_text(size = 14, color = "white"),
        # plot.subtitle = element_text(size = 18,family = "Elephant", color = "red"),
        axis.text = element_text(size = 12, color = "white"))+

  coord_cartesian(xlim = c(1,2), # This focuses the x-axis on the range of interest
                  ylim = c(1,2),
                  clip = 'off') +
  annotate(geom = "curve", 
           x = 2.1, y = 1.95, 
           xend = 1.95, yend = 2, 
           curvature = -.2, 
           color="white",
           size = 0.4,
           arrow = arrow(length = unit(2, "mm"))) +
  annotate(geom = "text",
           x = 2.12, y = 1.95,
           label = "Fortran 90",
           hjust = "left",
           color="white")+
  annotate(geom = "curve", 
           x = 2.1, y = 2, 
           xend = 2.04, yend = 2.04, 
           curvature = .2, 
           color="white",
           size = 0.4,
           arrow = arrow(length = unit(2, "mm"))) +
  annotate(geom = "text",
           x = 2.12, y = 2,
           label = "make",
           hjust = "left",
           color="white")
  
t

ggsave(filename = "treemap.png",t)







